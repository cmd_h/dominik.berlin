---
title: "Strunk, White, Angell: The Elements of Style (4th Edition)"
date: 2020-08-08T17:27:07+02:00
draft: false
---
One of my goals with this blog is to practice my writing skill. As a non-native
English speaker I have gone through years and years of school English, but that's been nearly
half a lifetime ago and when it comes to
grammar or punctuation rules I could have paid more attention to be honest. I additionally believe,
that acquiring a writing style requires a lot of time and is hard to train in school.

The Elements of Style is a book on this topic I've seen mentioned fairly often in blogs over the years,
most likely [daringfireball.net] and [kottke.org]. Since I started this page fairly recently, now
felt like a good time to read it.

It is a refreshingly short and quick read, but it's one of those books that has a lot of depth to it and
will warrant revisiting in the future. In other
words: don't expect my writing style to reflect all recommendations made in it right away.

The book contains five chapters, the first four are lists with advise on punctuation,
composition, form, and expressions. The last chapter is an essay on how to approach
acquiring your own writing style.

The fourth chapter on "Misused words and expressions" was the least interesting to me. A lot of the
problems described there, I never encountered or knew already (maybe that's the benefit of
being a non-native speaker, the stupid things are usually left out in school). To give an
example, the expression "I could care less" never
made any logical sense to me. If there is still room below your current level of "care", how
is that an expression for something you are **not** interested in.

The other three lists are really helpful, especially the 2nd chapter gave me some good pointers.
As a starting point I picked the following three rules to try and apply them to my writing and will
expand on them later on:

- Put Statements in positive form (ch. II rule 15)
- Use definite, specific, concrete language (ch. II rule 16)
- Omit needless words (ch. II rule 17)

The essay in the fifth chapter also made a lot of sense to me. A very brief summary is:
The more novice you are, the less you should deviate from the basic rules. Finding your own
voice is a process that will take time and practice (this sounds like solid advice in any
practice or skill).

All in all, I really liked that The Elements of Style gave me some concrete pointers where I
can improve my writing. That seems to be the best I can hope for without having a professional
editor at hand.

[daringfireball.net]: https://daringfireball.net
[kottke.org]: https://kottke.org
