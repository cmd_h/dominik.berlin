---
title: "Zeynep Tufekci: Twitter and Tear Gas"
date: 2020-07-14T13:29:10+02:00
draft: false
---

I've been following [Zeynep Tufekci on Twitter][@zeynep] for a while now and especially
liked her [recent][models]
[coverage][parks] on
[Covid-19][masks] for The Atlantic. Because of these articles I
looked more into her background and noticed that she has also written a book:
["Twitter and Tear Gas: The Power and Fragility of Networked Protests"](https://www.twitterandteargas.org/).

With the current reemergence of the #BlackLivesMatter protests and the too-many-to-list-here
political developments and scandals in recent years, where "social" media was often
involved, the title hooked me. I was not disappointed.

The book takes a look at the organisational structures of the protests during the arab
spring and occupy movements because the author has often first-hand experience of these
protests. I couldn't really tell if she was initially participating as an activist herself
or more
like a journalistic or academic observer, but in any case her analysis manages well to
take an unbiased
look and more of an observing viewpoint (at least for my leftist political views). She
tries to look at the strengths and weaknesses of the tools and forms of organization and not
condemn anything, which she also notes herself:

> "Any analysis must necessarily embrace this complexity and try to avoid the false dichotomies:
> optimists versus pessimists; utopians versus dystopians; humans versus technology." (p. 263)

What Zeynep Tufekci writes is not only interesting in regards to protest movements though, but
offers also
a lot of food for thought for someone like me, working in the IT industry. From the
examples she gives it is very clear that we IT professionals must do a better job
of considering the possible outcomes of the policies that we necessarily integrate
into the tools we are building and
how moving fast and breaking things might actually break things:

> "It's as if the networked public sphere, and indeed traditional institutions
> of democracy, can be DDOSed via releasing large numbers of flares, each
> attracting and consuming attention, thus making focus and sustained conversation
> impossible." (p. 274)

There is also a good bit in there that gave me the feeling of better understanding why
we have so many people screaming "Fake News" and purporting conspiracy theories:

> "The paralysis and disempowerment of doubt leads to the loss of credibility,
> spread of confusion, inaction and withdrawal from the issue by ordinary people,
> depriving movements of energy. If everything is in doubt, while the world is run
> by secret cabals that successfully manipulate everything behind the scenes, why bother?" (p. 250)

Maybe the line between fake and real is so hard to distinguish for some people that it really doesn't
matter anymore if something is a lie or the truth.

To be honest "Twitter and Tear Gas" wasn't a totally easy read for me, I'm more used to technical
literature
in English and the sociological nature of the text made progress a bit slower
and also necessitated some more diversions with other books than usual (it still reads
well though, just a lot to process).
In the end though, I can wholeheartedly recommend the book
to those who want to better understand how technology
and sociology interact in shaping our current political situation.

[@zeynep]: https://twitter.com/zeynep
[models]: https://www.theatlantic.com/technology/archive/2020/04/coronavirus-models-arent-supposed-be-right/609271/
[parks]: https://www.theatlantic.com/health/archive/2020/04/closing-parks-ineffective-pandemic-theater/609580/
[masks]: https://www.theatlantic.com/health/archive/2020/04/dont-wear-mask-yourself/610336/
