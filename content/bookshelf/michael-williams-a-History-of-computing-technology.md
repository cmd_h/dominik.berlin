---
title: "Michael Williams: A History of Computing Technology"
date: 2020-08-04T16:23:42+02:00
draft: false
---
With this book I can truly say that I **finally** took the time to read it. I can't tell the
exact year of purchase anymore, but it must have been at least ten years on my [Tsundoku][tsundoku] pile.
It did already have one purpose though, for some years it served me well to prop up my laptop a few centimeters.

Looking back at the history of things, even in such a young industry as computing, is often worthwhile
in my opinion. It avoids spending time on solving problems that have been solved long ago. As you can see from
my example though, it is quite hard to take the time away for a history lesson when there is so much
new content coming out every day.

Just because it
is still fresh on my mind: Uber Engineering apparently ran into this trap of reinventing the wheel fairly recently
with a blog post
titled ["Introducing Domain-Oriented
Microservice Architecture"][uber-archive] (that they took down in the meantime, so link goes to archive.org).
They caught a lot of ridicule on Twitter for the title because Microservices have been linked to domain-driven design
for years:

{{< tweet 1288072774083383297 >}}

Coming back to the book, it really is about "Computing". The text starts several thousand years ago with the
development of early number systems and the
difference between additive (e. g. Roman Numerals) and positional systems (e. g. what we use today) and then goes
into the depths of manual calculation aids like the Abacus or Napier`s Bones. A large focus is on mechanical devices
like the ones Charles Babbage tried to build in the 19th century or the relay based machines of the early 1940s by
Konrad Zuse, Bell Labs and Harvard. Only in the last two chapters the book turns to machines that we'd have a chance
to recognize as computers today.

This large focus on very early computing was unexpected to me, but was a welcome surprise. It's a similar feeling
to first learning about the inner workings of compilers (I still want to complete
the [Dragon Book][dragon-book] though). Understanding how the transformation from manual, to
mechanical, to electronic happened explains a lot about why things turned out like they have. If you can get your hands on
a copy of "Michael Williams: A History of Computing Technology", I can fully recommend spending some hours on looking
back on the history of computing.

[tsundoku]: https://en.wikipedia.org/w/index.php?title=Tsundoku&oldid=967992695
[uber-archive]: https://web.archive.org/web/20200725004555/https://eng.uber.com/microservice-architecture/
[dragon-book]: https://en.wikipedia.org/wiki/Compilers:_Principles,_Techniques,_and_Tools
