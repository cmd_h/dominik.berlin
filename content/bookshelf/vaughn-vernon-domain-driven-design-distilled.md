---
title: "Vaughn Vernon: Domain-Driven Design Distilled"
date: 2021-01-21T22:08:37+01:00
draft: false
---
When looking at books about Domain-Driven Design, the two most prominent are the original "blue book" (Domain-Driven Design by Eric Evans or DDD for short) and
the "red book" (Implementing Domain-Driven Design by Vaughn Vernon or IDDD for short). I own both of them, but only read "the original" DDD so far (in my defense, each of them has more than 500 pages).

I recently came across Domain-Driven Design Distilled (so, err, DDDD?) by Vaughn Vernon and thought I could use a refresher without committing to reading IDDD just yet. While it is likely written to serve mainly as an introductory text to the topic, it really gives a good concise overview of domain-driven design. And although I've taken a lot of thoughts and ideas from the original DDD book it definitely lacks in practical advice. This book manages to put a lot of that into its 136 pages.

The main topics are Bounded Contexts, Ubiquitous Language, Subdomains, Context Mapping, Aggregates and Domain Events. These really are the core elements of DDD and this is
actually quite hard to figure out with the blue book since there is so much more in it
as well.

I'm very much inclined to now also carve out some time to read IDDD in full.