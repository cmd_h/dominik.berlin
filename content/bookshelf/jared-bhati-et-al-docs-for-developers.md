---
title: "Jared Bhati et al.: Docs for Developers"
date: 2022-06-05T09:33:11+02:00
draft: false
---
Reading "Docs for Developers" is part of a larger theme for me in trying to create better
documentation for the software products I'm working on. Going into the specifics that
triggered this would go into too much depth here, but the major questions I'm currently
asking myself in this area are:

- How can documentation be kept better up-to-date in a large team?
- How do I mitigate the curse of knowledge and find out what I need to document for others?
- How can documentation (or existing tooling) be made easier to discover?
- How can we avoid introducing multiple sources of truth, e. g. when writing about details
  in the code?
- How can we avoid writing in too much detail about industry wide practices that are
  better explained in books or other canonical sources (link vs. write)?
- How can we incentivize documentation writing and archiving in teams? 

The focus of the book is on writing public documentation for software projects
with larger audiences and larger budgets or communities. Two chapters
describe how to do user research to determine what the audience needs and
how to measure documentation quality. There might be ways to do this in the small,
but for the internal docs I'm currently working on, this is not something I see
as a priority.

The most interesting chapters for me were the ones on drafting, editing, code examples and maintaining
documentation. Especially the emphasis on drafting first and editing as different 
phases is something I should take to heart. I'm getting better at this, but in the past
I often struggled with quick writing, because I was trying to write "perfectly" right
away, essentially doing both steps at once. Doing editing with multiple ordered passes,
focusing on technical accuracy, completeness, structure and clarity and brevity, is also
a suggestion I believe I can benefit from.

What was a bit underwhelming, was the content on information architecture. Just saying there are linear structures, hierarchies
and networks seemed a bit generic. Some more insights into common patterns specifically for technical
writing is something I would have liked to see.

I believe I could have also lived without the examples of the "Corg.ly" dog bark translation
service, that are used to introduce the chapters. It's hard to tell though whether that would
have made the reading more boring or only quicker.

In general I can recommend this book, it's an easy and quick read, gave a good overview
of the matter and had a few insights that gave my thinking about documentation more
clarity.