---
title: "Martin Fowler: Refactoring 2nd Edition"
date: 2020-12-28T13:47:05+01:00
draft: false
---
Several years ago I read the four introductory chapters of the 1st edition of Martin Fowlers classic [*Refactoring*][fowler-refactoring] and, as recommended by the author, only briefly skimmed the catalog of refactorings. Against the authors recommendation, I rarely looked into it afterwards though and mainly got my refactoring fix online from the great catalog at [sourcemaking.com][sourcemaking].

With the recent 2nd edition of *Refactoring* I took a different approach and read it cover to cover. Although the differences of the refactorings tends
to get a bit blurry after reading more than five or six at a time, I didn't regret reading every chapter. There is quite some interesting commentary especially in the example parts of the refactorings that discuss why "composition over inheritance" might be a bit too simplistic or why modularization is important for software design.

If you don't want to commit to reading the book completely, you will probably get 80% of the value by doing what Martin Fowler suggests: reading the first hundred pages, a few of the refactorings that capture your interest and skimming the headlines of the other ones. 

Between the two editions there are two major differences I noticed. The obvious one is the switch from Java to JavaScript for the examples. Interestingly to me this coincides with my own changes in programming languages. While reading the 1st edition I mainly programmed in Java and PHP, now it's mostly JavaScript and Python. The second major change I saw were a lot of renamings of the refactorings. These gave me the impression that Martin Fowler wanted to focus less on object-orientation and make the names more applicable to other styles like functional programming, e. g. "Extract Function" instead of "Extract Method" or "Introduce Special Case" instead of "Introduce Null Object".

What I really like about Martin Fowlers writing style is that he seems to be absolutely undogmatic about programming, except for focusing on clarity of intent. Every tool a language offers seems to be in his toolbox if it allows the program to be expressed more clearly.

The 2nd edition of *Refactoring* is still an absolute recommendation to read and one of the few books I'd consider must-reads for every programmer no matter the language.

[fowler-refactoring]: https://martinfowler.com/books/refactoring.html
[sourcemaking]: https://sourcemaking.com/refactoring