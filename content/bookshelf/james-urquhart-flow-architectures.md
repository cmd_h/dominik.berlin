---
title: "James Urquhart: Flow Architectures – The Future of Streaming and Event-Driven Integration"
date: 2021-06-22T21:39:18+02:00
draft: false
---
A recommendation for ["Flow Architectures" by James Urquhart][flow-architectures] crossed my Twitter stream a few months ago. With a topic in relation to streaming and event-driven architectures it was an instant buy for me.

I have been interested in what is called "Flow" here, since first hearing about CQRS and event sourcing some years ago. I believe there is a very logical benefit in storing events over, in essence, a snapshot of data at a point in time, like most systems do today. So far I didn't have a good enough reason to design a system that leverages events at its core though, but I want to be ready when the opportunity arises. 

James Urquhart describes a vision of "World Wide Flow" (WWF) analogous to the WWW. A global network where data flows in near real-time from producers to consumers, replacing the data batch processing of the past. He envisions a lot of societal changes and business development to go along with these mainly technological advances and I can see his point. Switching the business processes to really make use of these technology changes will be challenging. It reminded me a lot of a talk by Dan North I saw a few years ago: [How to Break the Rules][how-to-break-the-rules]

The style he uses for the fairly courageous thing of trying to predict the development of technology up to ten years into the future, is something I have never seen done before in a book. And I must say I would like to see this used as a template by other authors. Urquhart uses [Wardley Maps][wardley-maps] and [Promise Theory][promise-theory] to very methodically go through the reasoning for his predictions. Every step of the way is understandable and if necessary it would make it possible to argue or alter his work.

Regarding architecture, the book stays mostly in very high-level abstractions and doesn't offer much in terms of practical advice. There is just a discussion of existing technologies in the appendix, with its fifty page nearly a quarter of the book. This is understandable given the predictive nature of Urquharts vision and the speed with which technology evolves. I mainly prefer books this way. However, when ordering, I expected a somewhat more practical guide for system architecture and not a discussion of a system similar in size to the WWW.

I'm not disappointed though, the book really felt visionary at times. If the prediction Urquhart lays out really comes to fruition, I'm now well prepared. It's just something to be aware of when buying this book.

[how-to-break-the-rules]: https://gotober.com/2017/sessions/271/how-to-break-the-rules
[wardley-maps]: https://medium.com/wardleymaps
[promise-theory]: https://en.wikipedia.org/wiki/Promise_theory
[flow-architectures]: https://www.oreilly.com/library/view/flow-architectures/9781492075882/