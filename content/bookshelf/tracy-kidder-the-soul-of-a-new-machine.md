---
title: "Tracy Kidder the Soul of a New Machine"
date: 2020-09-05T22:42:50+02:00
draft: false
---
"The Soul of a New Machine" by Tracy Kidder is the next entry in my attempt to learn more about
computing history after recently reading [Michael Willams' "A History of Computing Technology"][history-of-computing].
Writing a review about a book that has already won the Pulitzer prize in 1982
could be somewhat futile considering the reach of my page and the myriads of reviews
that this book has likely garnered. I'd still like to write down some thoughts about
it though, even if they are just for me.

The book is a historical account of the building of a 32-bit computer called the
Eclipse MV/8000 (codename Eagle) by data general at the end of the 1970s. Its major
advancement and selling point for the company was that the instruction set is a superset of older
16-bit eclipse machines and therefore backwards compatible with prior made software.

It's not totally clear how historically accurate all the pieces are and whether there
are some fictional exaggerations in the book. From my personal experiences
in project work, the descriptions sound at least believable.

Although the topic might sound very technical, the book stays
very approachable to non-technical readers and mainly looks at the social
interactions
of the people that worked on it and the company politics of data general. Some
background knowledge of hardware architecture might help while reading, but the missing bits are
all explained in an understandable fashion and with just the right detail to no interrupt
the flow of the story.

Some large parts of the book describe the long working hours the engineers kept to
make the project succeed. This is kind
of a dichotomy I'm struggling with, great projects often seem to need these
types of overtime efforts, but burn out is still real and project workers cannot keep running forever.
Especially if the current efforts to
move from software project to continuously developed software products continues and there are no real down periods
between projects anymore, we need to keep a pace that is sustainable indefinitely.
I think it's good that this isn't shown off as too heroic in the text and that
the negative sides for the personal lives and health of the engineers are also discussed.

Again and again when reading historical books like this I'm surprised how prescient and nuanced
some of the fears about future developments were (this might be a form
of survivors bias though, that the books and people that made wrong predictions are
not available anymore). A recent example was in
"The Making of the Atomic Bomb" where the physicist that were building it in the
1940s are described as having foreseen very
well, how the atomic bomb would change the world. In this book the following lines
caught my attention:

> Norbert Weiner prophesied that the computer would offer "unbounded possibilities for
> good and for evil," and he advanced faintly, the hope that the contributors to this new
> science would nudge it in a humane direction. But he also invoked the fear that its
> development would fall "into the hands of the most irresponsible and venal of our
> engineers." One of the best surveys of the studies of the effects of computers ends
> with an appeal to the "computer professionals" that they exercise virtue and restraint.

This is more of an aside to the main story where the author veers a bit into a
philosophical discussion. However, it feels easily applicable to the ethical problems
that Facebook or Uber are showing.
Currently I fear that the above hasn't worked out in the way they had hoped for.

"The Soul of a New Machine" was a a nice leisure read, that I finished very quickly.
I can recommend it to anyone doing project work because even though it describes a hardware
project, the parallels with some of the
current software practices were interesting to look at -
although I'd recommend to
avoid project work if you can help it. While the book is probably not 100% historically accurate it was
a good way to get some more insights into hardware building and the state of mind
people were in at a time when so much development of the hardware industry was still ahead of it.

[history-of-computing]: {{< ref "/bookshelf/michael-williams-a-history-of-computing-technology" >}}
